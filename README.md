# README #

### This repository is meant to help set up AWS Marketplace [Personal Protective Equipments](https://aws.amazon.com/marketplace/pp/prodview-2inbkii6o24k4?ref_=srh_res_product_title)

For this reason it has methods for:
- loading model to instance
- preparing payload for request
- sending request
- displaying image with recognized people and confidence of PPE classes